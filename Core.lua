CreateFrame("Frame", "Rifter");

-- Initialize CallbackHandler-1.0 and tell it to name the functions RegisterCallback, UnregisterCallback and UnregisterAllCallbacks
Rifter.events = LibStub("CallbackHandler-1.0"):New(Rifter, "RegisterCallback", "UnregisterCallback", "UnregisterAllCallbacks");

-- Register the PLAYER_LOGIN event and trigger and start our loading process there
Rifter:RegisterEvent("PLAYER_LOGIN");

-- Declare a basic script that gets triggered OnEvent (when any event is triggered), if the event is PLAYER_LOGIN we're going to start initializing
Rifter:SetScript("OnEvent", function(self, event)
	if(event == "PLAYER_LOGIN") then
		--Trigger the callback that tells the rest of the core to initialize
		Rifter.events:Fire("InitializeCore");
		-- Trigger the callback that tells our modules to initialize
		Rifter.events:Fire("InitializeModules");
	end
end);
