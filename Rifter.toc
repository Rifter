## Interface: 30000
## Title: Rifter
## Notes: An infinite amount of *very* customizable buttons
## Author: Cncfanatics
## Version: alpha
## SavedVariables: RifterDB
## X-Category: Action Bars
## X-Donate: PayPal: diego.duclos@gmail.com
## X-Email: diego.duclos@gmail.com

embeds.xml

Core.xml
