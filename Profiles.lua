-- Initialize function
local characterID = ("%s - %s"):format(GetRealmName(), UnitName("player"));

local function Initialize()
	-- Make sure the required tables exist
	if(type(RifterDB) ~= "table") then
		RifterDB = {};
	end
	if(type(RifterDB.CurrentProfile) ~= "table") then
		RifterDB.CurrentProfile = {};
	end
	if(type(RifterDB.Profiles) ~= "table") then
		RifterDB.Profiles = {};
	end
	-- If the current character has no profile assigned to it, assign it a profile equal to his CharacterID (RealmName - CharName)
	Rifter:SetProfile(RifterDB.CurrentProfile[characterID] or characterID);
	Rifter.events:Fire("DatabaseReady");
end

-- Return the current profile, has a valid response only after DatabaseReady has triggered.
function Rifter:GetProfile()
	return RifterDB.CurrentProfile[characterID];
end

-- Set the current profile to the passed profileName
function Rifter:SetProfile(profileName)
	if(type(RifterDB.Profiles[profileName]) ~= "table") then
		RifterDB.Profiles[profileName] = {};
	end
	Rifter.CurrentProfile = RifterDB.Profiles[profileName];
	local oldProfile = RifterDB.CurrentProfile[characterID];
	RifterDB.CurrentProfile[characterID] = profileName;
	if(oldProfile ~= profileName) then
		Rifter.events:Fire("ProfileChanged", oldProfile, profileName);
	end
end

-- Register the InitializeCore callback on which we'll call our initializer function
Rifter.RegisterCallback("Profiles", "InitializeCore", Initialize);
